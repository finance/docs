Billing Service is a REST API which provides an interface for DoIT's Financial System, CBS.
This service allows DoIT Service Providers to send bills for their users to CBS.

The goal of this service is to consolidate billing processes that interact with CBS.
This will allow us to easily switch to a different financial application if CBS were to be replaced, without affecting the billing processes.

Our clients are SEO Service Providers for infrastructure services including Physical Server Hosting, Virtual Server Hosting, Bucky Backup, Enterprise Storage, and Cloud Services.

We also intend the Billing Service to be used for reporting purposes and customer inquiries.
Please see [Reporting Database](./docs/reporting-database.md) for more information.

[[_TOC_]]

### OpenAPI Specification

Here is a link to our OpenAPI Specification: https://api.test.billing.doit.wisc.edu/api-docs/
or navigate to the [UW Madison Developer Portal](https://developer.wisc.edu) to discover the Billing Service API.

The OpenAPI Specification includes descriptions of Billing Service resources and their properties, the available endpoints and supported methods, and examples of requests and responses.
This is the primary location for our API Documentation. Any changes to our API will first be reflected in the OpenAPI Specification.

The Swagger document above allows you to try out the API functionality as well.
You can select the environment you want to interact with and send requests.
You will need the appropriate access token to do so (Please read [Getting Access](./docs/getting-access.md)).

### Responsibilities of Billing Service

The Billing Service API has three resources, `Charge`, `FundingSource`, and `Service`.

#### Charge
The `Charge` resource represents a bill for a customer.

The `Charge` resource can represent a "Showback" or a "Chargeback" bill. This is indicated by the `billable` property on the resource.

The Billing Service is responsible for submitting valid "Chargeback" charges to CBS.

The Billing Service considers a charge valid if it meets the following criteria:
* The charge is billable (`billable` property is true).
* The charge is billed to an `active` and `chargeback` `FundingSource` resource (contains `fundingSourceId` property of an `active` and `chargeback` `FundingSource` resource).
* The charge is for a valid `Service` (contains `serviceId` property of a valid `Service`)

`Charge` resource submission to CBS happens asynchronously from when a user creates the `Charge` resource in the Billing Service.
This allows users of the Billing Service to create `Charge` resources without depending on the availability of CBS.

__Example__
```
{
  "amount": 450,
  "units": 2,
  "serviceId": "1234abcd-12ab-34cd-56ef-5678efgh90ij",
  "billable": true,
  "fundingSourceId": "5678efgh-12ab-34cd-56ef-1234abcd90ij",
  "serviceDate": "2020-01-01T00:00:00.000Z"
}
```

The Billing Service will append errors to the `errors` property of the `Charge` resource if there are issues with charge submission.
The errors will need to be resolved for the `Charge` resource to be submitted.

__Example__
```
{
  "id": "faeba731-9706-4080-83f7-d5f1c540b41a",
  "amount": 450,
  "units": 2,
  "serviceId": "1234abcd-12ab-34cd-56ef-5678efgh90ij",
  "billable": true,
  "fundingSourceId": "5678efgh-12ab-34cd-56ef-1234abcd90ij",
  "serviceDate": "2020-01-01T00:00:00.000Z",
  "createdDate": "2020-01-04T00:00:00.000Z",
  "errors": [
    {
      "code": 201,
      "message": "Funding Source is not active."
      "relatedResource": "http://api.test.billing.doit.wisc.edu/fundingSource/5678efgh-12ab-34cd-56ef-1234abcd90ij"
    }
  ]
}
```

The Billing Service checks if a submitted charge has been processed in CBS on an hourly basis.
If it has been processed, the `processedDate` property of the `Charge` resource will be added.
The resource itself will also be set to `readOnly` so that no further changes can be made to the resource.

__Example__
```
{
  "id": "faeba731-9706-4080-83f7-d5f1c540b41a",
  "amount": 450,
  "units": 2,
  "serviceId": "1234abcd-12ab-34cd-56ef-5678efgh90ij",
  "billable": true,
  "fundingSourceId": "5678efgh-12ab-34cd-56ef-1234abcd90ij",
  "serviceDate": "2020-01-01T00:00:00.000Z",
  "createdDate": "2020-01-04T00:00:00.000Z",
  "submittedDate": "2020-01-04T00:00:00.000Z",
  "processedDate": "2020-01-08T00:00:00.000Z",
  "readOnly": true
}
```

The Billing Service is not responsible for submitting credits for customers.
Please contact Financial Services for more information (projectbilling@doit.wisc.edu) regarding Customer Credits.

#### FundingSource
The `FundingSource` resource represents the funding string a bill is charged against.
A `FundingSource` can be one of two types, Dnumber/UDDS type or Project/Task type. They will also indicate if they can be used for Showback and/or Chargeback purposes.

__Example__

A `FundingSource` resource with a Dnumber and UDDS.
```
{
  "id": "5678efgh-12ab-34cd-56ef-1234abcd90ij",
  "name": "Division of Information technology:Enterprise Integrations",
  "chargeback": true,
  "showback": true,
  "dnumber": "D000101",
  "udds": "A001001",
  "active": true,
  "validationMessage": "Funding Source has been validated.",
  "createdDate": "2020-01-01T00:00:00.000Z"
}
```
__Example__

A `FundingSource` resource with a Project and Task.
```
{
  "id": "5678efgh-12ab-34cd-56ef-1234abcd90ij",
  "name": "CBS - Support",
  "chargeback": true,
  "showback": true,
  "project": "1001018",
  "task": "200",
  "active": true,
  "validationMessage": "Funding Source has been validated.",
  "createdDate": "2020-01-01T00:00:00.000Z"
}
```
The Billing Service contains an initial set of `FundingSource` resources with funding strings available in CBS.
The user submitting a charge is responsible for getting the correct funding string information from their customers.
If a `FundingSource` resource with the funding string exists in the Billing Service, its `id` property can be used in a `Charge` resource.

The Billing Service validates `FundingSource` resources against CBS.
If a `FundingSource` resource is not `active`, `Charge` resources using it will fail the submission process.

When creating Showback/Chargeback charges, they need to use the appropriate Showback/Chargeback `FundingSource`.

Billing Service clients are able to create (`POST /fundingSource`) Showback `FundingSource` resources as they see fit for reporting purposes.
However, clients are not allowed to create Chargeback `FundingSource` resources since CBS is the source of truth for this.

__Example__

Request body for `FundingSource` resource.
```
{
  "udds": "A001001",
  "dnumber": "0"
}
```

#### Service
The `Service` resource represents the service a bill is charged for.
The Billing Service contains an initial set of services registered in CBS.
The Billing Service validates `Service` resources against CBS and populates the `inventoryNumber` and `inventoryDescription` properties.
The `inventoryNumber` property is required for a `Service` resource to be valid.
This provides CBS information regarding where the revenue should go. 

Service owners are responsible for registering a service with CBS.

As a client you can `POST` new services with the required `inventoryNumber`, `inventoryDescription`, `billable`, and
`cbsName` properties.

The `inventoryNumber`, `inventoryDescription` should be available to Service Owners.

Financial Services team should have the correct `cbsName` value for the service in question.

__Example__

A `Service` resource with required properties.
```
{
  "cbsName": "STORAGE",
  "billable": true,
  "inventoryNumber": "70701",
  "inventoryDescription": "AWS Cloud Billing"
}
```

Please contact Financial Services for more information (projectbilling@doit.wisc.edu) regarding registering a Service.

### Resources

To further familiarize yourself with REST APIs please use the resources below:
* [REST API](https://www.restapitutorial.com/)
* [Using REST API](https://www.smashingmagazine.com/2018/01/understanding-using-rest-api/)
* [OpenAPI Specification](https://swagger.io/specification/#:~:text=Introduction,or%20through%20network%20traffic%20inspection.)

### Example

* Physical Hosting/CCI usage data is added to CMDB
* Charge amounts are calculated for customers based on their usage
  * Identify any charges that are missing required information to submit to API
* Charges are submitted to the billing service API
  * The IDs for services are saved in CMDB
  * The IDs for customer's funding sources are looked up before submitting the charge
  * Showback charges are submitted with billable set to false
  * Chargeback charges are submitted with billable set to true
* Chargeback charges are checked for updates to determine when they are submitted and processed by CBS
  * If a charge has an error and was unable to be submitted to CBS the error will be saved to a dashboard
* Errors are reviewed on the dashboard and resolved which causes the charge to be submitted
