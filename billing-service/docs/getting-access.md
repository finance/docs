## Getting Access

Please navigate to the [UW Madison Developer Portal](https://developer.wisc.edu) to discover the Billing Service API
and follow the instructions to access the Billing Service API.