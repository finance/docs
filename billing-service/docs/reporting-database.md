## Reporting Database

The Billing Service has a separate SQL based persistence layer for Reporting.
We synchronize the Reporting Database every hour.

- [Tables](#Tables)
    * [Charge](#Charge)
    * [Service](#Service)
    * [Funding Source](#Funding-Source)
- [Connect to Reporting Database through Google Data Studio](#Connect-to-Reporting-Database-through-Google-Data-Studio)
    * [Requesting Access](#Requesting-Access)

## Tables
Information about the tables and its contents are described below

### Charge

The `charges` table contains a copy of all charges in the Billing Service. 

| Column name | Description |
|---|---|
|`id`|Unique identifier for this charge.|
|`amount`|Amount of dollars due|
|`funding_source_id`|The ID of the funding source to be used for this charge.|
|`service_id`|The ID of the service this charge is tied to.|
|`created_date`|Date this charge resource was created in the Billing Service.|
|`service_date`|Date that service was provided.|
|`submitted_date`|Date when this charge was submitted to the external financial system.|
|`processed_date`|Date when this charge was processed in the external financial system.|
|`billable`|Whether this charge is eligible for submission to external financial system.|
|`read_only`|Read only state will be true if the charge has been processed in the external financial system.|
|`cbs_id`|ID for the charge in CBS (external financial system).|
|`line_number`|Line number for a charge of a CBS invoice.|


### Service

The `services` table contains a copy of all services in the Billing Service.

| Column name | Description |
|---|---|
|`id`|Unique identifier for this service.|
|`name`|Descriptive name of the service|
|`cbs_name`|Name used in CBS (external financial system) tables|
|`inventory_number`|Identifier used by financial services for funding location.|
|`inventory_description`|Description of service the inventory number is tied to.|
|`created_date`|Date this service service was created in the Billing Service.|
|`billable`|Whether charges for this service are eligible for submission to external financial system|
|`unit`|Denomination for which the service is quantified|


### Funding Source

The `funding_sources` table contains a copy of all funding Sources in the Billing Service.

| Column name | Description |
|---|---|
|`id`|Unique identifier for this funding source.|
|`dnumber`|DoIT Number - a type of funding string which is always tied to a UDDS.|
|`udds`|Unit, Division, Department, Subdepartment.|
|`project`|Project - a type of funding string.|
|`task`|Task - a type of funding string which is always tied to a Project.|
|`parent_id`|The parent dnumber/udds funding source ID that a project/task funding source will charge to.|
|`valid`|Whether the record is valid for purposes of showback or chargeback reporting.|
|`validation_message`|Information about validation problems.|
|`name`|Descriptive name of this funding source.|
|`created_date`|Date this funding source resource was created in the Billing Service.|


### Charge Error

The `charge_errors` table contains Errors for Charges in the Billing Service.

| Column name | Description |
|---|---|
|`charge_id`|`id` of the charge with this error.|
|`code`|Billing Service internal error code.|
|`message`|Information about this error.|
|`related_resource`|Link to the resource this error is related to.|


## Connect to Reporting Database through Google Data Studio

You can use the PostgreSQL connector in Google Data Studio to connect to the Reporting Database.
(_Please see:_ https://support.google.com/datastudio/answer/7288010?hl=en&ref_topic=7332343)

### Requesting Access

If you are looking to access the Billing Service Reporting Database please email us (doit-billing-service-support@office365.wisc.edu).

Please include the following information in the email:

* Your name
* Your email address
* The name of your department
* The reason for requesting access

Once your request is reviewed and approved, we will notify you and provide you the required credentials.